<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario2 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio2">

    <?php $form = ActiveForm::begin();

        echo $form
                ->field($model, 'nombre') // campo a mostrar
                ->textInput( // mostrar como caja de texto
                        ['placeholder' => "Escriba su nombre"] // opciones
                        );
        
        echo $form
                ->field($model, 'poblacion') // campo
                ->dropDownList( // lista desplegable
                            $model->poblaciones(), // lista de de datos
                            ['prompt' => "Selecciona una poblacion"] // opciones
                        ); 
        
        echo $form
                ->field($model,'color') // campo
                ->listBox($model->colores()); // cuadro de lista
                
        
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio2 -->
