<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario1 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio1">

    <?php $form = ActiveForm::begin(); ?>

        <?php 
            echo $form  
                    ->field($model, 'nombre') // el campo a utilizar
                    ->textInput([
                        "placeholder" => "Introduce nombre completo"
                    ]);  // colocar caja de texto
        
            echo $form
                    ->field($model, 'direccion') // campo
                    ->textarea([
                        "cols" => 30,
                        "rows" => 5
                    ]); // colocar area de texto
            
            
            echo $form
                    ->field($model, 'edad') // campo
                    ->input("number"); // input type=number
            
            
            echo $form
                    ->field($model, 'fecha') // campo
                    ->input("date");  // input type=date
        ?>
    
    
    <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio1 -->
