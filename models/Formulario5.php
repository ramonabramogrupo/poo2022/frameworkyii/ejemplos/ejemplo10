<?php

namespace app\models;

class Formulario5 extends \yii\base\Model{
    public string $nombre="";
    public string $apellidos="";
    public string $email="";
    public string $fecha="";
    public string $poblacion="";
    public array $meses=[];
    private string $mesTexto="";
    
    public function rules(): array {
        return [
            [['nombre','apellidos'],'required'],
            [['email'],'email'],
            [['fecha','poblacion','meses'],'safe'],
        ];
    }
    
    public function attributeLabels(): array {
        return [
          "nombre" => "Nombre",
          "apellidos" => "Apellidos",
          "email" => "Correo Electronico",
          "fecha" => "Fecha de Nacimiento",
          "poblacion" => "Poblacion" ,
          "meses" => "Meses de acceso"
        ];
    }
    
    public function getMesTexto(): string {
        return join(",",$this->meses);
    }
    
    public function mesesMostrar(){
        return [
            "junio" => "Junio",
            "julio" => "Julio",
            "agosto" => "Agosto",
            "septiembre" => "Septiembre"
        ];
    }
    
    public function poblacionesMostrar(){
        return [
            "laredo" => "Laredo",
            "santander" => "Santander",
            "astillero" => "Astillero"
        ];
    }


    
    

}
